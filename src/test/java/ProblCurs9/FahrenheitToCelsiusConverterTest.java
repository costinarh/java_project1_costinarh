package ProblCurs9;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FahrenheitToCelsiusConverterTest {
    FahrenheitToCelsiusConverter converter;
    @Before
    public void setUp() {
        converter = new FahrenheitToCelsiusConverter();
    }
    @Test
    public void toCelsius() {
        double grade = 150;
        double result = converter.fahrenheitToCelsius (grade);
        Assert.assertEquals(65.55,result,0.01);
    }
    @Test
    public void toFahrenheit() {
        double grade = 30;
        double result = converter.celsiusToFahrenheit (grade);
        Assert.assertEquals(86,result,0.01);
    }


}
