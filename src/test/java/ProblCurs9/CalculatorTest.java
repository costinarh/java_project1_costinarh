package ProblCurs9;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)

public class CalculatorTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none ();

    private int nr1;
    private int nr2;
    private Calculator calculator;

    @Mock
    private FahrenheitToCelsiusConverter converter;

    @Before
    public void setUp() {
    calculator = new Calculator (converter);

    }


    @org.junit.Test
    public void add () {

//        Setare
        nr1=5;
        nr2=7;

//        Executare
        int total = calculator.add(nr1, nr2);
//        Verificare
        assertEquals (12,total);

    }

    @org.junit.Test
    public void dif () {

//        Setare (given)
        nr1=9;
        nr2=5;

//      Executare (When)
    int valoare = calculator.dif (nr1, nr2);

//      Verificare
        assertEquals (4,valoare);


    }
    @Test
    public void givenNumberWhenDivideBy0ThanExceptionIsThrown() {
//            Setup
        int nr = 10;
        expectedException.expect (ArithmeticException.class);
           expectedException.expectMessage ("/ by zero");

//        Execute
        int val = calculator.div (nr, 0);
//        Verify

    }
    @Test
    public void toCelsius() {
        double grade = 150;
        double result = converter.fahrenheitToCelsius (grade);
        Assert.assertEquals(65.55,result,0.01);
    }
    @Test
    public void toFahrenheit() {
        double grade = 30;
        when(converter.celsiusToFahrenheit(grade)).thenReturn ((double) 86);
        double result = converter.celsiusToFahrenheit (grade);
        assertEquals(86,result,0.01);
        verify(converter).celsiusToFahrenheit(grade);

    }


}