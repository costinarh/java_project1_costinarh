package ProblCurs9;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith (Parameterized.class)

public class CalculatorParameterisedTest {
    public static Integer[][] matrix = new Integer[][]{
            {5,10,15},
            {-7,-9,-16},
            {-5,0,-5}

    };
    @Parameterized.Parameters(name="{index}:number1={0},number2={1},expectedResult={2}")
    public static Collection <Integer[]> parameters(){
        return Arrays.asList (matrix);
    }
    @Parameterized.Parameter()
    public int number1;
    @Parameterized.Parameter(1)
    public int number2;
    @Parameterized.Parameter(2)
    public int expectedResult;

    @Test
    public void  givenNumbersWhenAddThenResultShouldBeAsExpected(){
        Calculator calculator = new Calculator (new FahrenheitToCelsiusConverter());
        int actualResult = calculator.add(number1,number2);
        Assert.assertEquals (expectedResult, actualResult);

    }

}
