import java.util.Scanner;

public class ex14_tema {
//    14)Scrieti un program care sa calculeze si sa afiseze perimetrul si aria unei figuri geometrice introdusa de la tastatura.
//        Pe langa denumirea figurii geometrice se vor mai primi de la tastatura si lungimea, latimea, inaltimea sau raza in functie
//        de caz. Programul trebuie sa ofere suport macar pentru urmatoarele figuri: cerc, patrat, dreptunghi, triunghi.
    public static void main (String[] args) {
        double aria=0.0;
        double lungimea=0.0;

        System.out.println ("Introdu forma geometrica pe care doresti sa o calculezi:");
        System.out.println ("Pentru Cerc introdu 1 ");
        System.out.println ("Pentru Patrat introdu 2 ");
        System.out.println ("Pentru Dreptunghi introdu 3 ");
        System.out.println ("Pentru Triunghi introdu 4 ");
        Scanner scanner = new Scanner (System.in);
        char Chr1 = (char) scanner.nextInt ();
        switch (Chr1) {
            case 1:
                System.out.println ("Introdu raza cercului: ");
                double raza = scanner.nextDouble ();
                aria =  3.14 * raza * raza ;
                lungimea = 2 * 3.14 * raza ;
                System.out.println ("Aria cercului este: " + aria);
                System.out.println ("Lungimea cercului este: " + lungimea);
                break;
            case 2:
                System.out.println ("Introdu latura patratului: ");
                double latura = scanner.nextDouble ();
                aria =  latura * latura / 2 ;
                lungimea = latura * 4 ;
                System.out.println ("Aria patratului este: " + aria);
                System.out.println ("Lungimea patratului este: " + lungimea);
                break;
            case 3:
                System.out.println ("Introdu lungimea dreptunghiului: ");
                double lung = scanner.nextDouble ();
                System.out.println ("Introdu latimea dreptunghiului: ");
                double lat = scanner.nextDouble ();
                aria =  lung * lat ;
                lungimea = (lung + lat) * 2 ;
                System.out.println ("Aria dreptunghiului este: " + aria);
                System.out.println ("Lungimea dreptunghiului este: " + lungimea);
                break;
            case 4:
                System.out.println ("Introdu baza triunghiului : ");
                double baza = scanner.nextDouble ();
                System.out.println ("Introdu o cateta : ");
                double cateta1 = scanner.nextDouble ();
                System.out.println ("Introdu cealalta cateta a triunghiului : ");
                double cateta2 = scanner.nextDouble ();
                System.out.println ("Introdu inaltimea dreptunghiului: ");
                double inalt = scanner.nextDouble ();
                aria =  (baza * inalt) / 2 ;
                lungimea = (baza + cateta1 + cateta2) ;
                System.out.println ("Aria dreptunghiului este: " + aria);
                System.out.println ("Lungimea dreptunghiului este: " + lungimea);
                break;
            default:
                break;




        }

    }
}
