import java.util.Scanner;

public class ex7_tema {
    public static void main (String[] args) {
        System.out.println ("Introdu marimea array-ului: ");
        Scanner scanner = new Scanner (System.in);
        int nr = scanner.nextInt ();
        int[] intArray = new int[nr];
        int sum = 0;
        int aux = 0;

        System.out.println ("Elementele initiale ale Array-ului");
        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = scanner.nextInt ();
        }
        for (int i = 0; i < intArray.length; i++) {
            sum += intArray[i];
        }
        System.out.println (arrayMax (intArray) - arrayMin (intArray));
    }

    public static int arrayMin (int[] ar) {
        int min = ar[0];
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] < min) {
                min = ar[i];
            }
        }
    return min;
    }
    public static int arrayMax (int[] ar) {
        int max = ar[0];
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] > max) {
                max = ar[i];
            }
        }
        return max;
    }
}


