import java.util.Scanner;

public class ex13_tema {
//    13)Write a Java program to return the sum of the digits present in the given string. If there is no digits the sum return is 0.
//    Go to the editor

    public static void main (String[] args) {

        Scanner scanner = new Scanner (System.in);
        System.out.println ("Introdu un string: ");
        String string = scanner.nextLine ();
        System.out.println("Suma cifrelor este: " + calcul(string));

    }
    public static int calcul (String inputString) {
        int lungime = inputString.length ();
        int sum = 0;
        for (int i = 0; i < lungime; i++) {
            if (Character.isDigit (inputString.charAt (i))) {
               sum = sum + Character.getNumericValue (inputString.charAt (i));

            }
        }
        return sum;
    }
}
