import java.util.Scanner;

public class ex10_tema {
    public static void main (String[] args) {


            System.out.println ("Introdu un sir de caractere: ");
            Scanner scanner = new Scanner (System.in);
            String vChr1 = scanner.nextLine ();
            System.out.println ("Introdu caracterul de inlocuit: ");
            String chrOld = scanner.nextLine ();
            String new_vChr1 = vChr1.replace(chrOld, "");
            System.out.println ("Sintaxa initiala este : " + vChr1);
            System.out.println ("Sintaxa inlocuita este : " + new_vChr1);

    }
}
