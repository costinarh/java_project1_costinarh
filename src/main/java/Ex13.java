import java.util.Scanner;

public class Ex13 {

        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            System.out.print("Citeste stringul: ");
            String sline = sc.nextLine();
            System.out.println("Numarul cerut este: " + intify(sline));
        }

        public static int intify(String x)
        {
            int stringToInt = 0;
            for (int i=0; i<x.length(); i++)
            {
                if (x.charAt(i) >= '0' && x.charAt(i) <= '9')
                {
                    stringToInt += Character.getNumericValue(x.charAt(i));
                }
            }
            return stringToInt;
        }
    }



