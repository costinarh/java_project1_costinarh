import java.util.Scanner;

public class ex2 {
    public static void main (String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Numarul de impartit in cifre: ");
        int nr = sc.nextInt();
        System.out.println("Cifre individuale:");
        for (int i=0; i<separate(nr).length; i++)
        {
            System.out.print(separate(nr)[i] + " ");
        }
    }
    public static int[] separate(int x)
    {
        int aux = x;

        int counter = 0;
        while (aux !=0)
        {
            aux = aux / 10;

            counter++;
        }
        int[] numbers = new int[counter];
        for (int i = counter-1; i >= 0; i--)
        {
            numbers[i] = x % 10;
            x = x / 10;
        }
        return numbers;


    }

}
