import java.util.Scanner;

public class ex8_tema {
    public static void main (String[] args) {
        System.out.println ("Introdu marimea array-ului: ");
        Scanner scanner = new Scanner (System.in);
        int nr = scanner.nextInt ();
        int[] intArray = new int[nr];
        int sum = 0;
        int aux = 0;
        double avg = 0;

        System.out.println ("Elementele initiale ale Array-ului");
        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = scanner.nextInt ();
        }
        for (int i = 0; i < intArray.length; i++) {
            sum += intArray[i];
        }
        System.out.println (arrayMax (intArray) + " " + arrayMin (intArray));
        avg = (sum-(arrayMax (intArray)+arrayMin (intArray)))/(intArray.length-2);
        System.out.println ("Media valorilor exceptand minim si maxim: "+ avg);
    }

    public static double arrayMin (int[] ar) {
        int min = ar[0];
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] < min) {
                min = ar[i];
            }
        }
        return min;
    }
    public static double arrayMax (int[] ar) {
        int max = ar[0];
        for (int i = 0; i < ar.length; i++) {
            if (ar[i] > max) {
                max = ar[i];
            }
        }
        return max;
    }
    public static double arrayMediu (int[] ar) {
        int sum = 0;
        for (int i = 0; i < ar.length; i++) {
            sum += ar[i];
        }
    return sum;

    }

}
