import java.util.Scanner;

public class ex5_tema {
    public static void main (String[] args) {

        System.out.println ("Introdu marimea array-ului: ");
        Scanner scanner = new Scanner (System.in);
        int nr = scanner.nextInt ();
        int[] intArray = new int[nr];
        int sum = 0;
        int avg =0;

        System.out.println ("Elementele initiale ale Array-ului");
        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = scanner.nextInt ();
        }
        for (int i = 0; i < intArray.length; i++) {
            sum += intArray[i];
        }
        avg = sum/intArray.length;
        System.out.println ("Average value is: "+ avg);
    }
}