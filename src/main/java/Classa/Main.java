package Classa;

import java.util.Scanner;

public class Main {
    public static void main (String[] args) {
        Scanner scanner = new Scanner (System.in);
        System.out.println ("Introdu nume: ");
        String numeleMeu  = scanner.nextLine ();
        System.out.println("Introdu prenume: ");
        String prenumeleMeu  = scanner.nextLine ();
        Person cineva = new Person(numeleMeu, prenumeleMeu);
        System.out.println (cineva.getNume ()+ " " + cineva.getPrenume ());
        Person cineva2 = new Classa.Person ();
        cineva2.setNume ("Popescu");
        cineva2.setPrenume ("Gigel");
        System.out.println (cineva2.getNume () + " " + cineva2.getPrenume ());
        cineva2.setPrenume ("Maria");
        System.out.println (cineva2.getNume () + " " + cineva2.getPrenume ());


    }
}
